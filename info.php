<?php
error_reporting(E_ALL);
header("Content-Type: text/plain"); 

require_once("read_plugin.php");

$plugin_base_dir = dirname(__FILE__)."/plugins";

if ( !isset($_REQUEST['plugin']) )
{
    echo "{}";
    die;
}

$plugin_name = $_REQUEST['plugin'];
$plugin_dir = isset($_REQUEST['dir']) ? $_REQUEST['dir'] : $_REQUEST['plugin'];
$plugin_api_version = isset($_REQUEST['api']) ? $_REQUEST['api'] : 0;


$plugin = read_plugin("$plugin_base_dir/$plugin_dir/plugin_$plugin_name.json");

if ( !$plugin )
    echo "{}";
else
{
    echo json_encode($plugin);
}